# README #

Script for Auto Scaling WVD/RDS Fall 2019 release (Spring 2020 is coming, hold your breath) Host Pool Session Hosts. 
This script requires the following modules.

* Az

* Microsoft.RDInfra.RDpowershell

To use this module, 

0. Install the required modules.

1. Drop this module's files in C:\Users\<service account>\WindowsPowerShell\Modules\Wvd-AutoScale\

2. (Optional) Register a new event source. "New-EventLog -Source 'Wvd-AutoScale' -LogName Application" (If you don't it'll just print stuff to screen. Syslog forwarding is a forthcoming feature.)

3. Fill in the Wvd_org.ps1 file. 

	a. Leave the defaults alone. Or don't. Whatever. 

	b. Fill in your 'Org' data. This must include a tenant name, two credential file paths (unless you want to run it interactively), your AAD Tenant ID (primarily used for connecting to RDS as a tenant admin), and your subscription id. 

	c. You can pretty much null or empty any field within the org, and the defaults should take over.

	d. The credential files can be anywhere, but be sure to restrict permissions to the minimum. 
	
	e. The credential files should contain two lines, first line is username/app id, second line is password. If you store a secure string value as your service account, you should be able to avoid having a plain password in a file. 

4. Run it.

	a. Logged in as a service account, open powershell and run "wvd-autoscale -Verbose".
	
	b. Take note of the recommendations. Those are the actions the script will take when it's really set loose. 

5. Really run it.

	a. When you're ready, run "Wvd-AutoScale -Verbose -Simulation:$false". Things will happen.

6. Automate it.

	a. Setup a scheduled task to run the above command regularly, like every 5 minutes. 

I'm not a git guru, but I think you can 'git stash Wvd_org.ps1' and 'git stash pop' it when you come back for updates. 

The above stated, keep a backup copy of your filled in Wvd_org.ps1 file. 

Be aware: this script currently uses 'Active Sessions' to scale out. 

If you haven't already, I recommend you set a GPO to auto logout/disconnect inactive users after a period of time. This will help allow for features like scaling and complete shutdown to behave appropriately.

When you find issues, please submit them to me. If you think of cool features that are missing, please submit them to me. I will try to address them when I can. 