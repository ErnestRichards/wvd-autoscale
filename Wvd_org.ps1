<# Wvd_org.ps1 #>
# defines your globals

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$Organizations = @{

	# Add your organization data here by copying or filling in the default org.
	# 
	# you may rename this org, and you may use arbitrary names.
	#  by default, this script will try to run against all of the orgs;
	#  alternatively, you can specify an org name
	# this must include a tenant name and subscription id,
	#  but should also include credential file paths.
	# credential files should include a single
	#  user name (or app id) on the first line and
	#  password (plain text or system-protected) on the second line.
	# if your credentials are service principal creds in the
	#  context of rds/wvd tenant administration, you must also include 
	#  the relavent AAD Tenant Id.
	# you may add all of these settings as 'global' settings in
	#  the lower section of this document, and they will be defaulted to
	#  if ommitted from an organization.
	# the script will attempt to default to the 'global' configurations
	#  if any invalid, null, or empty settings are defined in an org.
	#

	'MyOrg'=@{
		'TenantName'='Default Tenant'
		'HostPoolName'=''
		"AzureCredentialFilePath" = "C:\path\to\azure.creds"
		'TenantCredentialFilePath' = "C:\path\to\tenant.creds"
		'TenantId' = '00000000-1111-2222-3333-555555555555'
		'SubscriptionId' = '00000000-1111-2222-3333-555555555555'

		'AzureAsPlainText' = $false
		'TenantAsPlainText' = $false

		'AzureServicePrincipal' = $true
		'TenantServicePrincipal' = $true

		'ScaleOutMethod' = ""
		'ScaleOutThreshold' = .9
		'ScaleOutBufferType' = ""
		'ScaleOutBuffer' = $null
		'ScaleInMethod' = ""
		'ScaleInThreshold' = $null
		'MinHostCount' = 1
		'MinHostDays' = 'MoTuWeThFr'
		'MinHostHours' = '0545-1815'

		'DeploymentUrl' = ""
		'ExcludeTag' = ''
		'AllowCompleteShutdown' = $true

		'LogAnalyticsWorkspaceName' = ''
		'LogAnalyticsWorkspaceResourceGroupName' = ''
		'MonitorAfterHours' = $false
	}

}

$global:DefaultAzureCredentialFilePath = ""
$global:DefaultTenantCredentialFilePath = ""
$global:DefaultTenantId = ''
$global:DefaultSubscriptionId = ''

$global:DefaultAzureAsPlainText = $false
$global:DefaultTenantAsPlainText = $false

$global:DefaultAzureServicePrincipal = $false
$global:DefaultTenantServicePrincipal = $false

$global:DefaultScaleOutMethod = "PerCpu"
$global:DefaultScaleOutThreshold = .8
$global:DefaultScaleOutBufferType = "Sessions"
$global:DefaultScaleOutBuffer = 1
$global:DefaultScaleInMethod = "PerCpu"
$global:DefaultScaleInThreshold = .2
$global:DefaultMinHostCount = 1
$global:DefaultMinHostDays = 'MoTuWeThFr'
$global:DefaultMinHostHours = '0645-1615'

$global:DefaultDeploymentUrl = "https://rdbroker.wvd.microsoft.com"
$global:DefaultExcludeTag = 'ExcludeAutoScale'
$global:DefaultAllowCompleteShutdown = $false

$global:DefaultLogAnalyticsWorkspaceName = ''
$global:DefaultLogAnalyticsWorkspaceResourceGroupName = ''
$global:DefaultMonitorAfterHours = $false
$global:DefaultLogAnalyticsWorkspaceSubscriptionId = 'SubscriptionId'
$global:LawApiVersion = "2017-01-01-preview" # required for Log Analytics queries
