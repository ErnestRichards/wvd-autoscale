# TODO: (in progress) add syslog function (if syslog server defined in org/default, send log of session)
# TODO: allow for spring 2020 az/wvd api version
# TODO: allow scaleout based on current sessions, rather than active sessions
# TODO: email sfter hours users that a system will be starting soon # credit: eric debord
# TODO: Add scaleout by Percent
# TODO: add success verification (system is now running and available/system is no longer running)
# TODO: add handling of 'broken' systems (on but unavailable boxes with no sessions should probably be shutdown at least once)
# TODO: optimize selection of systems to start/stop based on cpu capacity
# TODO: allow for other api version in log analytics requests
# TODO: allow for after hours log check timespan?
# TODO: add 'getmaxusersessions' function to check max capacity based on current configurations
# TODO: add 'getusersessions' function
# TODO: add 'getsessionhosts' function
# TODO: add 'clearusersessions' function
# TODO: add 'clearsessionhostsessions' function
# TODO: add 'maintenancemode' function to block new sessions, add exclusion tag, warnings/blocks if sessions exist.
# TODO: add 'maintainsessionhost' function to run updates, install software, reboot system
# TODO: add exclude auto scale timeblock/expiration
# TODOne: account for 'allownewsession' setting on session hosts
# TODOne: watch for recent session attempts after hours, start a system
# TODOne: allow for complete shutdown of all systems outside of working hours
# TODOne: allow enumeration of all tenants
# TODOne: allow enumeration of all host pools
# TODOne: allow for multiple configs in the config file for separate tenants/HPs in separate subscriptions/aad tenants
# TODOne: Account for scaling optout tags
# TODOne: allow for service principal credentials in files

$ExecutionContext.SessionState.Module.OnRemove = {
    try { Remove-Module Wvd_org ; } catch {}
}

# Very basic. Assumes only a password in a file, or line 1 = username and line 2 = password
function GetCredential-FromFile {
	param(
		$Path,
		$Username = $env:username,
		[Switch] $AsPlainText
	)
	$contents = (gc $Path).split()
	if ($contents.Count -eq 1) {
		try {
			$err = $null
			$pwd = convertto-securestring "$($contents[0])" -AsPlainText:$AsPlainText -Force:$AsPlainText -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
			if ($err) {
				throw $err
			}
		} catch {
			$pwd = $null
		}
		$contents[0] = ""
		$contents = $null
	} elseif ($contents.Count -eq 2) {
		$Username = "$($contents[0])"
		try {
			$err = $null
			$pwd = convertto-securestring "$($contents[1])" -AsPlainText:$AsPlainText -Force:$AsPlainText -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
			if ($err) {
				throw $err
			}
		} catch {
			$pwd = $null
		}
		$contents[0] = ""
		$contents[1] = ""
		$contents = $null
	}
	if ($pwd -and $pwd.gettype().name -eq 'SecureString') {
		$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username, $pwd
	} else {
		$contents = ""
		$Username = ""
		$pwd = ""
		$contents = $null
		$Username = $null
		$pwd = $null
		throw 'No valid password discovered'
	}
	$contents = ""
	$Username = ""
	$pwd = ""
	$contents = $null
	$Username = $null
	$pwd = $null
	return $Credential
}

function Get-Creds ($AzureFile=$global:DefaultAzureCredentialFilePath, $AzurePlainText = $global:DefaultAzureAsPlainText, $TenantFile=$global:DefaultTenantCredentialFilePath, $TenantPlainText = $global:DefaultTenantAsPlainText) {
	if (!$global:AzureCredentials) {
		if ($AzureFile) {
			if ($AzurePlainText) {
				try {
					$global:AzureCredentials = GetCredential-FromFile $AzureFile -AsPlainText
				} catch {
					Write-Log "An error occurred when getting Azure credentials from file ($AzureFile - plain text): $_"
					$global:AzureCredentials = $null
				}
			} else {
				try {
					$global:AzureCredentials = GetCredential-FromFile $AzureFile
				} catch {
					Write-Log "An error occurred when getting Azure credentials from file ($AzureFile - secure string): $_"
					$global:AzureCredentials = $null
				}
			}
			if (!$global:AzureCredentials) {
				throw "I can't proceed without Azure credentials"
			}
		} else {
			Write-Host "[.] Collecting Azure Credentials..."
			try {
				$global:AzureCredentials = Get-Credential -Message "Enter Azure Active Directory Domain" -UserName "$Env:username@csub.onmicrosoft.com"
			} catch {
				Write-Log "An error occurred while getting Azure credentials interactively: $_"
				$global:AzureCredentials = $null
			}
			if (!$global:AzureCredentials) {
				throw "I can't proceed without Azure credentials"
			}
		}
	}
	if (!$global:TenantCredentials) {
		if ($TenantFile) {
			if ($TenantPlainText) {
				try {
					$global:TenantCredentials = GetCredential-FromFile $TenantFile -AsPlainText
				} catch {
					Write-Log "An error occurred when getting tenant credentials from file ($TenantFile - plain text): $_"
					$global:TenantCredentials = $null
				}
			} else {
				try {
					$global:TenantCredentials = GetCredential-FromFile $TenantFile
				} catch {
					Write-Log "An error occurred when getting tenant credentials from file ($TenantFile - secure string): $_"
					$global:TenantCredentials = $null
				}
			}
			if (!$global:TenantCredentials) {
				throw "I can't proceed without Tenant credentials"
			}
		} else {
			Write-Host "[.] Collecting Tenant Credentials..."
			try {
				$global:TenantCredentials = Get-Credential -Message "Enter Azure WVD Tenant Credentials" -UserName "$Env:username@csub.onmicrosoft.com"
			} catch {
				Write-Log "An error occurred while getting tenant credentials interactively: $_"
				$global:TenantCredentials = $null
			}
			if (!$global:TenantCredentials) {
				throw "I can't proceed without Tenant credentials"
			}
		}
	}
}

function Register-LogSource ($LogName = "Application", $Source = "Wvd-AutoScale") {
	try {
		$ret = New-EventLog -Log "$Application" -Source "$Source" -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
	} catch {}
}

function Write-Log ($message, $level = "Information", $eventid = 1337, $LogName = "Application", $Source = "Wvd-AutoScale" ) {
	try {
    	Write-EventLog -LogName $LogName -Source $Source -EntryType $level -EventId $eventid -Message $message -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -ErrorVariable err
    	if ($err) {
    		throw $err
    	}
	} catch {
		Register-LogSource $LogName $Source
		try {
	    	Write-EventLog -LogName $LogName -Source $Source -EntryType $level -EventId $eventid -Message $message -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -ErrorVariable err
	    	if ($err) {
	    		throw $err
	    	}
    	} catch {
			Write-Host "[!] Unable to write to event log. You may need to register this log source under an elevated context since I couldn't do it myself (New-EventLog -Log Application -Source 'Wvd-AutoScale'). Error: $_"
			Write-Host "Printing here instead:"
			Write-Host "$message"
		}
	}
}

function Login-Az ($Credentials = $global:AzureCredentials, $TenantId = $DefaultTenantId, $SubscriptionId = $COSubscriptionId, [Switch]$ServicePrincipal = $global:AzureServicePrincipal) {
	try {
		if ($TenantId -and $SubscriptionId) {
			$ctx = Set-AzContext -TenantId $TenantId -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue -ErrorVariable err
		} elseif ($SubscriptionId) {
			$ctx = Set-AzContext -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue -ErrorVariable err
		} elseif ($TenantId) {
			$ctx = Set-AzContext -TenantId $TenantId -ErrorAction SilentlyContinue -ErrorVariable err
		}
		if ($err) {
			throw $err
		}
	} catch {
		Import-Module Az -Force
		try {
			if ($TenantId -and $SubscriptionId) {
				$ctx = Set-AzContext -TenantId $TenantId -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue -ErrorVariable err
			} elseif ($SubscriptionId) {
				$ctx = Set-AzContext -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue -ErrorVariable err
			} elseif ($TenantId) {
				$ctx = Set-AzContext -TenantId $TenantId -ErrorAction SilentlyContinue -ErrorVariable err
			}
			if ($err) {
				throw $err
			}
		} catch {
			$ctx = $null
		}
	}
	if (!$ctx) {
		if (!$Credentials) {
			$Credentials = Get-Credential -Message "Azure Admin Creds"
			$global:AzureCredentials = $Credentials
		}
		if ($ServicePrincipal) {
			if ($TenantId -and $SubscriptionId) {
				$ctx = Login-AzAccount -Credential $Credentials -TenantId $TenantId -SubscriptionId $SubscriptionId -ServicePrincipal -ErrorAction SilentlyContinue -ErrorVariable err
			} elseif ($SubscriptionId) {
				$ctx = Login-AzAccount -Credential $Credentials -SubscriptionId $SubscriptionId -ServicePrincipal -ErrorAction SilentlyContinue -ErrorVariable err
			} elseif ($TenantId) {
				$ctx = Login-AzAccount -Credential $Credentials -TenantId $TenantId -ServicePrincipal -ErrorAction SilentlyContinue -ErrorVariable err
			} else {
				$ctx = Login-AzAccount -Credential $Credentials -ServicePrincipal -ErrorAction SilentlyContinue -ErrorVariable err
			}
			if ($err) {
				throw "Unable to log in: $err"
			}
		} else {
			if ($TenantId -and $SubscriptionId) {
				$ctx = Login-AzAccount -Credential $Credentials -TenantId $TenantId -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue -ErrorVariable err
			} elseif ($SubscriptionId) {
				$ctx = Login-AzAccount -Credential $Credentials -SubscriptionId $SubscriptionId -ErrorAction SilentlyContinue -ErrorVariable err
			} elseif ($TenantId) {
				$ctx = Login-AzAccount -Credential $Credentials -TenantId $TenantId -ErrorAction SilentlyContinue -ErrorVariable err
			} else {
				$ctx = Login-AzAccount -Credential $Credentials -ErrorAction SilentlyContinue -ErrorVariable err
			}
			if ($err) {
				throw "Unable to log in: $err"
			}
		}
	}
	if (!$ctx) {
		throw 'Unable to login to Azure, but im not sure why. Make sure you have installed and imported the Az Powershell module, and you are using good credentials.'
	}
	return $ctx
}

# TODO: Convert to take all object attributes
function HostPoolStatus-ToString ($HostPool) {
	$ret = ""
	try {
		$ret += "Tenant:`n  $(($HostPool.Value)[0].'Tenant Name')"
		$ret += "`nHost Pool:`n  $($HostPool.Name)"
		$ret += "`nSession Hosts:"
		foreach ($sessionhost in $HostPool.Value) {
			$ret += "`n-  Session Host Name`n    $($sessionhost.'Session Host Name')"
			$ret += "`n-  Total Sessions`n    $($sessionhost.'Total Sessions')"
			$ret += "`n-  Active Sessions`n    $($sessionhost.'Active Sessions')"
			$ret += "`n-  Allow New Sessions`n    $($sessionhost.AllowNewSession)"
			$ret += "`n-  Status`n    $($sessionhost.'Status')"
			$ret += "`n-  Power State`n    $($sessionhost.'Power State')"
			$ret += "`n-  Resource Group Name`n    $($sessionhost.'Resource Group Name')"
			$ret += "`n-  CPU Count`n    $($sessionhost.'CPU Count')`n"
			$ret += "`n-  Tags:`n"
			$sessionhost.Tags.Keys | %{ $ret += "      $_  -  $($sessionhost.Tags["$_"])`n" }
		}
	} catch {
		$ret = "[e] Bad Hostpool object. Should be a single dictionary object containing a list."
	}
	return $ret
}

function Wvd-AutoScale {
	[CmdletBinding()]
	param(
		[Parameter(ValueFromPipeline=$true,Position=0)]
		$Orgs,
		[Switch]$NoWait = $false, # for jobs to finish/vms to start
		[Switch]$Simulation = $true
	)

	$myorgs = @()

	if ($Orgs) {
		$ttype = $Orgs.GetType().Name
		if ($ttype -eq "String") {
			$myorgs += $Orgs
		} elseif ($ttype -eq "Object[]") {
			$myorgs = $Orgs
		} else {
			throw "Unsupported parameter type: $ttype"
		}
	} else {
		$myorgs = @($Organizations.Keys)
	}

	Write-Verbose "[.] Beginning org processing"

	$myorgs | %{

		$orgname = $_

		try {
			$org = $Organizations["$orgname"]
		} catch {
			throw "No Such Target ($orgname) defined in Wvd_org.ps1. Error message: $_"
		}

		Write-Verbose "[.] Processing organization $orgname"

		try { if ($org['TenantAsPlainText'] -eq $true -or $org['TenantAsPlainText'] -eq $false) { $tapt = $org['TenantAsPlainText']; } else {$tapt = $global:DefaultTenantAsPlainText; } } catch { $tapt = $global:DefaultTenantAsPlainText; }
		try { if ($org['AzureAsPlainText'] -eq $true -or $org['AzureAsPlainText'] -eq $false) { $aapt = $org['AzureAsPlainText']; } else {$aapt = $global:DefaultAzureAsPlainText; } } catch { $aapt = $global:DefaultAzureAsPlainText; }

		try { if ($org['TenantCredentialFilePath'] -and $org['TenantCredentialFilePath'] -ne '') { $tcf = $org['TenantCredentialFilePath']; if (!$tcf -or $tcf -eq '') { Write-Host "[w] Warning: Tenant Credential File not specified" } ; $tc = GetCredential-FromFile -Path $org['TenantCredentialFilePath'] -AsPlainText:$tapt ; } else { $tcf = $global:DefaultTenantCredentialFilePath; if (!$tcf -or $tcf -eq '') { Write-Host "[w] Warning: Tenant Credential File not specified" } ; $tc = GetCredential-FromFile -Path $global:DefaultTenantCredentialFilePath -AsPlainText:$tapt ; } } catch { $tcf = $global:DefaultTenantCredentialFilePath; if (!$tcf -or $tcf -eq '') { Write-Host "[w] Warning: Tenant Credential File not specified" } ; $tc = GetCredential-FromFile -Path $global:DefaultTenantCredentialFilePath -AsPlainText:$tapt ; }
		try { if ($org['AzureCredentialFilePath'] -and $org['AzureCredentialFilePath'] -ne '') { $acf = $org['AzureCredentialFilePath']; if (!$acf -or $acf -eq '') { Write-Host "[w] Warning: Azure Credential File not specified" } ; $ac = GetCredential-FromFile -Path $org['AzureCredentialFilePath'] -AsPlainText:$aapt ; } else { $acf = $global:DefaultAzureCredentialFilePath; if (!$acf -or $acf -eq '') { Write-Host "[w] Warning: Azure Credential File not specified" } ; $ac = GetCredential-FromFile -Path $global:DefaultAzureCredentialFilePath -AsPlainText:$aapt ; } } catch { $acf = $global:DefaultAzureCredentialFilePath; if (!$acf -or $acf -eq '') { Write-Host "[w] Warning: Azure Credential File not specified" } ; $ac = GetCredential-FromFile -Path $global:DefaultAzureCredentialFilePath -AsPlainText:$aapt ; }
		
		if (!$acf) {
			Write-Host "[w] Warning: Azure Credential File not defined"
		}
		if (!$tcf) {
			Write-Host "[w] Warning: Tenant Credential File not defined"
		}

		try { if ($org['TenantName']) { $tn = $org['TenantName']; } else { $tn = ''; } } catch { $tn = ''; }
		try { if ($org['TenantServicePrincipal'] -eq $true -or $org['TenantServicePrincipal'] -eq $false) { $tsp = $org['TenantServicePrincipal']; } else {$tsp = $global:DefaultTenantServicePrincipal; } } catch { $tsp = $global:DefaultTenantServicePrincipal; }
		try { if ($org['AzureServicePrincipal'] -eq $true -or $org['AzureServicePrincipal'] -eq $false) { $asp = $org['AzureServicePrincipal']; } else {$asp = $global:DefaultAzureServicePrincipal; } } catch { $asp = $global:DefaultAzureServicePrincipal; }
		try { if ($org['HostPoolName'] -and $org['HostPoolName'] -ne '') { $hpn = $org['HostPoolName']; } else {$hpn = ''; } } catch { $hpn = ''; }
		try { if ($org['ScaleOutMethod'] -and $org['ScaleOutMethod'] -ne '') { $som = $org['ScaleOutMethod']; } else {$som = $global:DefaultScaleOutMethod; } } catch { $som = $global:DefaultScaleOutMethod; }
		try { if ($org['ScaleOutThreshold'] -and $org['ScaleOutThreshold'] -gt 0) { $sot = $org['ScaleOutThreshold']; } else {$sot = $global:DefaultScaleOutThreshold; } } catch { $sot = $global:DefaultScaleOutThreshold; }
		try { if ($org['ScaleOutBufferType'] -and $org['ScaleOutBufferType'] -ne '') { $sobt = $org['ScaleOutBufferType']; } else {$sobt = $global:DefaultScaleOutBufferType; } } catch { $sobt = $global:DefaultScaleOutBufferType; }
		try { if ($org['ScaleOutBuffer'] -and $org['ScaleOutBuffer'] -gt 0) { $sob = $org['ScaleOutBuffer']; } else {$sob = $global:DefaultScaleOutBuffer; } } catch { $sob = $global:DefaultScaleOutBuffer; }
		try { if ($org['ScaleInMethod'] -and $org['ScaleInMethod'] -ne '') { $sim = $org['ScaleInMethod']; } else {$sim = $global:DefaultScaleInMethod; } } catch { $sim = $global:DefaultScaleInMethod; }
		try { if ($org['ScaleInThreshold'] -and $org['ScaleInThreshold'] -gt 0) { $sit = $org['ScaleInThreshold']; } else {$sit = $global:DefaultScaleInThreshold; } } catch { $sit = $global:DefaultScaleInThreshold; }
		try { if ($org['MinHostCount'] -and $org['MinHostCount'] -ge 0) { $mh = $org['MinHostCount']; } else {$mh = $global:DefaultMinHostCount; } } catch { $mh = $global:DefaultMinHostCount; }
		try { if ($org['MinHostDays'] -and $org['MinHostDays'] -ne '') { $mhd = $org['MinHostDays']; } else {$mhd = $global:DefaultMinHostDays; } } catch { $mhd = $global:DefaultMinHostDays; }
		try { if ($org['MinHostHours'] -and $org['MinHostHours'] -ne '') { $mhh = $org['MinHostHours']; } else {$mhh = $global:DefaultMinHostHours; } } catch { $mhh = $global:DefaultMinHostHours; }
		try { if ($org['SubscriptionId'] -and $org['SubscriptionId'] -ne '') { $sid = $org['SubscriptionId']; } else {$sid = $global:DefaultSubscriptionId; } } catch { $sid = $global:DefaultSubscriptionId; }
		try { if ($org['DeploymentUrl'] -and $org['DeploymentUrl'] -ne '') { $du = $org['DeploymentUrl']; } else {$du = $global:DefaultDeploymentUrl; } } catch { $du = $global:DefaultDeploymentUrl; }
		try { if ($org['TenantId'] -and $org['TenantId'] -ne '') { $aid = $org['TenantId']; } else {$aid = $global:DefaultTenantId; } } catch { $aid = $global:DefaultTenantId; }
		try { if ($org['ExcludeTag'] -and $org['ExcludeTag'] -ne '') { $et = $org['ExcludeTag']; } else {$et = $global:DefaultExcludeTag; } } catch { $aid = $global:DefaultExcludeTag; }
		try { if ($org['AllowCompleteShutdown'] -eq $true -or $org['AllowCompleteShutdown'] -eq $false) { $acs = $org['AllowCompleteShutdown']; } else {$acs = $global:DefaultAllowCompleteShutdown; } } catch { $acs = $global:DefaultAllowCompleteShutdown; }

		#if a syslog server is defined, use it. otherwise, just log locally. 
		try { if ($org['SyslogServer'] -and $org['SyslogServer'] -ne '') { $ss = $org['SyslogServer']; } else {$ss = $global:DefaultSyslogServer; } } catch { $ss = $null; }

		# handle after hours log analytics checking
		try { if ($org['MonitorAfterHours'] -eq $true -or $org['MonitorAfterHours'] -eq $false) { $ah = $org['MonitorAfterHours'];  } else {$ah = $global:DefaultMonitorAfterHours; } } catch { $ah = $global:DefaultMonitorAfterHours; }
		try { if ($org['LogAnalyticsWorkspaceResourceGroupName'] -and $org['MonitorAfterHours'] -ne '') { $wsrg = $org['LogAnalyticsWorkspaceResourceGroupName'];  } else {$wsrg = $global:DefaultLogAnalyticsWorkspaceResourceGroupName; } } catch { $wsrg = $global:DefaultLogAnalyticsWorkspaceResourceGroupName; }
		try { if ($org['LogAnalyticsWorkspaceName'] -and $org['LogAnalyticsWorkspaceName'] -ne '') { $wsn = $org['LogAnalyticsWorkspaceName'];  } else {$wsn = $global:DefaultLogAnalyticsWorkspaceName; } } catch { $wsn = $global:DefaultLogAnalyticsWorkspaceName; }
		try { if ($org['LogAnalyticsWorkspaceSubscriptionId'] -and $org['LogAnalyticsWorkspaceSubscriptionId'] -ne '') { $wssid = $org['LogAnalyticsWorkspaceSubscriptionId'];  } else {$wssid = $global:DefaultLogAnalyticsWorkspaceSubscriptionId; } } catch { $wssid = $global:DefaultLogAnalyticsWorkspaceSubscriptionId; }
		if ($wssid.ToLower() -eq 'subscriptionid') {
			$wssid = $sid
		}

		try {
			if ($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent) {
				Tenant-AutoScale -TenantName $tn -HostPoolName $hpn -ScaleOutMethod $som -ScaleOutThreshold $sot -ScaleOutBufferType $sobt -ScaleOutBuffer $sob -ScaleInMethod $sim -ScaleInThreshold $sit -MinHostCount $mh -TenantCreds $tc -TenantServicePrincipal:$tsp -AzureCreds $ac -AzureServicePrincipal:$asp -SubscriptionId $sid -DeploymentUrl $du -AADTid $aid -NoWait:$NoWait -Simulation:$Simulation -ExcludeTag $et -AzurePlainText:$aapt -TenantPlainText:$tapt -AzureCredentialFilePath $acf -TenantCredentialFilePath $tcf -MinHostHours $mhh -MinHostDays $mhd -AllowCompleteShutdown:$acs -SyslogServer $ss -MonitorAfterHours $ah -LawName $wsn -LawResourceGroupName $wsrg -LawSubscriptionId $wssid -ScaleOnTotalSessions:$true -Verbose
			} else {
				Tenant-AutoScale -TenantName $tn -HostPoolName $hpn -ScaleOutMethod $som -ScaleOutThreshold $sot -ScaleOutBufferType $sobt -ScaleOutBuffer $sob -ScaleInMethod $sim -ScaleInThreshold $sit -MinHostCount $mh -TenantCreds $tc -TenantServicePrincipal:$tsp -AzureCreds $ac -AzureServicePrincipal:$asp -SubscriptionId $sid -DeploymentUrl $du -AADTid $aid -NoWait:$NoWait -Simulation:$Simulation -ExcludeTag $et -AzurePlainText:$aapt -TenantPlainText:$tapt -AzureCredentialFilePath $acf -TenantCredentialFilePath $tcf -MinHostHours $mhh -MinHostDays $mhd -AllowCompleteShutdown:$acs -SyslogServer $ss -MonitorAfterHours $ah -LawName $wsn -LawResourceGroupName $wsrg -LawSubscriptionId $wssid -ScaleOnTotalSessions:$true
			}
		} catch {
			Write-Host "[e] Error running Wvd-AutoScale on org $orgname : $_"
			Write-Host "Command was: Tenant-AutoScale -TenantName $tn -HostPoolName $hpn -ScaleOutMethod $som -ScaleOutThreshold $sot -ScaleOutBufferType $sobt -ScaleOutBuffer $sob -ScaleInMethod $sim -ScaleInThreshold $sit -MinHostCount $mh -TenantCreds $tc -TenantServicePrincipal:$tsp -AzureCreds $ac -AzureServicePrincipal:$asp -SubscriptionId $sid -DeploymentUrl $du -AADTid $aid -NoWait:$NoWait -Simulation:$Simulation -ExcludeTag $et -AzurePlainText:$aapt -TenantPlainText:$tapt -AzureCredentialFilePath $acf -TenantCredentialFilePath $tcf -MinHostHours $mhh -MinHostDays $mhd -AllowCompleteShutdown:$acs -SyslogServer $ss -MonitorAfterHours $ah -LawName $wsn -LawResourceGroupName $wsrg -LawSubscriptionId $wssid -Verbose:$Verbose"
		}
	}
}

function Outside-WorkingHours ($SessionStartDate, $MinHostDays, $MinHostHours) {
	return ((($MinHostDays.ToLower() -match "$(($SessionStartDate.DayOfWeek.tostring()[0..1] -join '').ToLower())" -and ((Get-Date $SessionStartDate -Format "HHmm") -lt ($MinHostHours -split '-',2)[0] -or (Get-Date $SessionStartDate -Format "HHmm") -gt ($MinHostHours -split '-',2)[1]) -or $MinHostDays.ToLower() -notmatch "$(($SessionStartDate.DayOfWeek.tostring()[0..1] -join '').ToLower())")))
}

# ScaleOutMethod ['PerCpu','Offset']
# ScaleOutBufferType ['CPUs','Sessions']

function Tenant-AutoScale {
	[CmdletBinding()]
	param(
		[Parameter(ValueFromPipeline=$true,Position=0)]
		$TenantName = '',
		$HostPoolName = $null,
		$ScaleOutMethod = $global:DefaultScaleOutMethod,
		$ScaleOutThreshold = $global:DefaultScaleOutThreshold,
		$ScaleOutBufferType = $global:DefaultScaleOutBufferType,
		$ScaleOutBuffer = $global:DefaultScaleOutBuffer,
		$ScaleInMethod = $global:DefaultScaleInMethod,
		$ScaleInThreshold = $global:DefaultScaleInThreshold,
		$MinHostCount = $global:DefaultMinHostCount,
		$MinHostDays = $global:DefaultMinHostDays,
		$MinHostHours = $global:DefaultMinHostHours,
		$TenantCreds = $global:TenantCredentials,
		[Switch]$TenantServicePrincipal = $global:DefaultTenantServicePrincipal,
		[Switch]$TenantPlainText = $global:DefaultTenantAsPlainText,
		$TenantCredentialFilePath = $global:DefaultTenantCredentialFilePath,
		$AzureCreds = $global:AzureCredentials,
		[Switch]$AzureServicePrincipal = $global:DefaultAzureServicePrincipal,
		[Switch]$AzurePlainText = $global:DefaultAzureAsPlainText,
		$AzureCredentialFilePath = $global:DefaultAzureCredentialFilePath,
		$SubscriptionId = $global:DefaultSubscriptionId,
		$DeploymentUrl = $global:DefaultDeploymentUrl,
		$AADTid = $global:DefaultTenantId,
		$ExcludeTag = $global:DefaultExcludeTag,
		$AllowCompleteShutdown = $global:DefaultAllowCompleteShutdown,
		$SyslogServer = $null,
		$MonitorAfterHours = $global:DefaultMonitorAfterHours,
		$LawName = $global:DefaultLogAnalyticsWorkspaceName,
		$LawResourceGroupName = $global:DefaultLogAnalyticsWorkspaceResourceGroupName,
		$LawSubscriptionId = $global:DefaultLogAnalyticsWorkspaceSubscriptionId,
		[Switch]$ScaleOnTotalSessions = $false, # scale on total number of sessions or active sessions only (default)
		[Switch]$NoWait = $false, # for jobs to finish/vms to start
		[Switch]$Simulation = $true
	)

	$SessionStartDate = Get-Date
	$SessionId = [guid]::NewGuid().ToString()
	$SessionLog = @()
	$SessionLog += "Session Start"
	$SessionLog += "$SessionStartDate"
	$SessionLog += "Session ID: $SessionId"
	$SessionLog += "Session Run By $env:username"
	$SessionLog += "Session Run On $env:computername"
	$EventId = 1335
	
	$WriteToSyslog = $false
	if ($SyslogServer) {
		# try to process syslog server settings. if successful, set WriteToSyslog to true
		# examples:
		# 192.168.1.5
		# 192.168.1.5:514
		# 192.168.1.5:tcp/515

	}

	try {
		if ($Simulation) {
			Write-Host "[.] Working (Simulation/Recommendation Mode)"
			$SessionLog += "[.] Working (Simulation/Recommendation Mode)"
			$EventId = 1336
		} else {
			Write-Host "[.] Working"
			$SessionLog += "[.] Working"
			$EventId = 1337
		}

		Write-Verbose "[.] Using the following parameters:"
		$SessionLog += "[.] Using the following parameters:"
		Write-Verbose "[-] Scale Out Method: $ScaleOutMethod"
		$SessionLog += "[-] Scale Out Method: $ScaleOutMethod"

		if ($ScaleOutMethod.tolower() -eq 'percpu') {
			Write-Verbose "[-] Threshold: >= $ScaleOutThreshold Sessions Per CPU"
			$SessionLog += "[-] Threshold: >= $ScaleOutThreshold Sessions Per CPU"
		} elseif ($ScaleOutMethod.tolower() -eq 'offset') {
			Write-Verbose "[-] Threshold: >= $ScaleOutThreshold Sessions less than the number of CPUs"
			$SessionLog += "[-] Threshold: >= $ScaleOutThreshold Sessions less than the number of CPUs"
		} else {
			throw "Invalid Scale Out Method. Use PerCpu or Offset."
		}

		Write-Verbose "[-] Scale In Method: $ScaleInMethod"
		$SessionLog += "[-] Scale In Method: $ScaleInMethod"

		if ($ScaleInMethod.tolower() -eq 'percpu') {
			Write-Verbose "[-] Threshold: <= $ScaleInThreshold Sessions Per CPU"
			$SessionLog += "[-] Threshold: <= $ScaleInThreshold Sessions Per CPU"
		} elseif ($ScaleInMethod.tolower() -eq 'offset') {
			Write-Verbose "[-] Threshold: <= $ScaleInThreshold Sessions less than the number of CPUs"
			$SessionLog += "[-] Threshold: <= $ScaleInThreshold Sessions less than the number of CPUs"
		} else {
			throw "Invalid Scale In Method. Use PerCpu or Offset."
		}

		$UnsetTenantCreds = $false
		$UnsetAzureCreds = $false

		if (!$TenantCreds -and !$AzureCreds) {
			$global:TenantCredentials = $null
			$global:AzureCredentials = $null

			Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
			$TenantCreds = $global:TenantCredentials
			$AzureCreds = $global:AzureCredentials
		
			if (!$TenantCreds) {
				throw 'No tenant creds, no work'
			}
			if (!$AzureCreds) {
				throw 'No azure creds, no work'
			}
			$UnsetTenantCreds = $true
			$UnsetAzureCreds = $true
		} elseif (!$TenantCreds) {
			$global:TenantCredentials = $null
			Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
			$TenantCreds = $global:TenantCredentials
		
			if (!$TenantCreds) {
				throw 'No tenant creds, no work'
			}
			$UnsetTenantCreds = $true
		} elseif (!$AzureCreds) {
			$global:AzureCredentials = $null
			Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
			$AzureCreds = $global:AzureCredentials

			if (!$AzureCreds) {
				throw 'No azure creds, no work'
			}
			$UnsetAzureCreds = $true
		}

		Write-Verbose "[-] Azure Credentials: $($AzureCreds.UserName)"
		$SessionLog += "[-] Azure Credentials: $($AzureCreds.UserName)"

		if ($AzureCreds.UserName -notlike '*@*' -and $AzureCreds.UserName -notlike '*-*-*-*-*') {
			Write-Verbose "[w] Warning - the Azure credentials you have provided do not appear to be app ids or usernames with domains, so they may not work"
			$SessionLog += "[w] Warning - the Azure credentials you have provided do not appear to be app ids or usernames with domains, so they may not work"
		}

		Write-Verbose "[-] Azure Service Principal: $AzureServicePrincipal"
		$SessionLog += "[-] Azure Service Principal: $AzureServicePrincipal"

		Write-Verbose "[-] Tenant Credentials: $($TenantCreds.UserName)"
		$SessionLog += "[-] Tenant Credentials: $($TenantCreds.UserName)"

		if ($TenantCreds.UserName -notlike '*@*' -and $TenantCreds.UserName -notlike '*-*-*-*-*') {
			Write-Verbose "[w] Warning - the Tenant credentials you have provided do not appear to be app ids or usernames with domains, so they may not work"
			$SessionLog += "[w] Warning - the Tenant credentials you have provided do not appear to be app ids or usernames with domains, so they may not work"
		}

		Write-Verbose "[-] Tenant Service Principal: $TenantServicePrincipal"
		$SessionLog += "[-] Tenant Service Principal: $TenantServicePrincipal"

		Write-Verbose "[-] Subscription ID: $SubscriptionId"
		$SessionLog += "[-] Subscription ID: $SubscriptionId"

		if (!$SubscriptionId -or $SubscriptionId -eq '') {
			throw "Add your Azure Subscription ID in your Wvd_org.ps1 file!"
		}

		Write-Verbose "[-] AAD Tenant ID: $AadTid"
		$SessionLog += "[-] AAD Tenant ID: $AadTid"

		if (!$AadTid -or $AadTid -eq '') {
			throw "Add your Azure AD Tenant ID in your Wvd_org.ps1 file!"
		}

		$Tenants = @()

		if ($TenantName -and $TenantName -ne '') {
			$Tenants += $TenantName
		} else {
			Write-Verbose "[.] Logging into RDS"
			$SessionLog += "[.] Logging into RDS"

			if ($TenantServicePrincipal) {
				try {
					$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ServicePrincipal -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
					if ($err) {
						throw $err;
					}
				} catch {
					if (!$UnsetTenantCreds) {
						$global:TenantCredentials = $null
						Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
						$TenantCreds = $global:TenantCredentials
					
						if (!$TenantCreds) {
							throw "No tenant creds, no work, after trying to reset credentials due to a signin error. check your credentials/cred files. related (probably useless) error message: $_"
						}
						$UnsetTenantCreds = $true
						try {
							$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ServicePrincipal -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
							if ($err) {
								throw $err;
							}
						} catch {
							throw "An error occurred on RDS Signin after reimporting credentials: $_"
						}
					} else {
						throw "An error occurred on RDS Signin: $_"
					}
				}
			} else {
				try {
					$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
					if ($err) {
						throw $err;
					}
				} catch {
					if (!$UnsetTenantCreds) {
						$global:TenantCredentials = $null
						Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
						$TenantCreds = $global:TenantCredentials
					
						if (!$TenantCreds) {
							throw "No tenant creds, no work, after trying to reset credentials due to a signin error. check your credentials/cred files. related (probably useless) error message: $_"
						}
						$UnsetTenantCreds = $true
						try {
							$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
							if ($err) {
								throw $err;
							}
						} catch {
							throw "An error occurred on RDS Signin after reimporting credentials: $_"
						}
					} else {
						throw "An error occurred on RDS Signin: $_"
					}
				}
			}
			if (!$RdsContext -or $err) {
				throw "Error on RDS Signin: $err"
			}

			Write-Verbose "[.] Logged into RDS"
			$SessionLog += "[.] Logged into RDS"

			$Tenants = @((Get-RdsTenant).TenantName | Sort-Object)
		}

		Write-Verbose "[-] Minimum Host Count: $MinHostCount"
		$SessionLog += "[-] Minimum Host Count: $MinHostCount"

		Write-Verbose "[-] Allow Complete Shutdown: $AllowCompleteShutdown"
		$SessionLog += "[-] Allow Complete Shutdown: $AllowCompleteShutdown"

		if ($AllowCompleteShutdown -eq $true) {
			Write-Verbose "[-] Minimum Host Days: $MinHostDays"
			$SessionLog += "[-] Minimum Host Days: $MinHostDays"

			Write-Verbose "[-] Minimum Host Hours: $MinHostHours"
			$SessionLog += "[-] Minimum Host Hours: $MinHostHours"
		}

		Write-Verbose "[-] WVD Tenant Name: $TenantName"
		$SessionLog += "[-] WVD Tenant Name: $TenantName"

		Write-Verbose "[-] WVD Host Pool Name: $HostPoolName"
		$SessionLog += "[-] WVD Host Pool Name: $HostPoolName"

		Write-Verbose "[-] Write session log to syslog server(s): $WriteToSyslog"
		$SessionLog += "[-] Write session log to syslog server(s): $WriteToSyslog"

		if ($WriteToSyslog -eq $true) {
			Write-Verbose "[-] Syslog Servers ($(@($SyslogServer).count)):"
			$SessionLog += "[-] Syslog Servers ($(@($SyslogServer).count)):"
			@($SyslogServer) | %{
				Write-Verbose "[.]   $_"
				$SessionLog += "[.]   $_"
			}
		}

		$OutsideWorkingHours = Outside-WorkingHours $SessionStartDate $MinHostDays $MinHostHours

		Write-Verbose "[.] Currently outside working hours: $OutsideWorkingHours"
		$SessionLog +=  "[.] Currently outside working hours: $OutsideWorkingHours"

		Write-Verbose "[.] Monitor After Hours Connection Attempts: $MonitorAfterHours"
		$SessionLog +=  "[.] Monitor After Hours Connection Attempts: $MonitorAfterHours"
		
		$RecentConns = @()

		if ($OutsideWorkingHours -and $MonitorAfterHours) {
			if (!$LawName -or $LawName -eq '' -or !$LawSubscriptionId -or $LawSubscriptionId -eq '' -or !$LawResourceGroupName -or $LawResourceGroupName -eq '') {
				Write-Verbose "[e] No log analytics workspace information was provided. I won't be able to monitor for after hours connections as desired."
				$SessionLog +=  "[e] No log analytics workspace information was provided. I won't be able to monitor for after hours connections as desired."
				$MonitorAfterHours = $false
			} else {
				Write-Verbose "[.] Checking for after hours session attempts"
				$SessionLog +=  "[.] Checking for after hours session attempts"
				# there's probably a better way to do this
				if ($AzureServicePrincipal) {
					$tRecentConns = Get-ConnectionAttempts -WorkspaceName $LawName -LawSubscriptionId $LawSubscriptionId -ResourceGroupName $LawResourceGroupName -AzureCreds $AzureCreds -AzureServicePrincipal -AzureSubscriptionId $SubscriptionId -AADTid $AadTid
				} else {
					$tRecentConns = Get-ConnectionAttempts -WorkspaceName $LawName -LawSubscriptionId $LawSubscriptionId -ResourceGroupName $LawResourceGroupName -AzureCreds $AzureCreds -AzureSubscriptionId $SubscriptionId -AADTid $AadTid
				}
				foreach ($t in $tRecentConns) {
					$RecentConns += PsObjectTo-HashTable $t
				}
				$RecentConnCount = $RecentConns.Count
				Write-Verbose "[.] $($RecentConns.Count) recent after hours session attempts"
				$SessionLog +=  "[.] $($RecentConns.Count) recent after hours session attempts"
				if ($RecentConns.Count -gt 0) { # dump full value, or just a subset? TESTME
					foreach ($key in $RecentConns.Keys) {
						$SessionLog += "$key - $($RecentConns[$key])";
					}
				}
			}
		}

		Write-Verbose "[.] Processing $($Tenants.Count) Tenant(s)"
		$SessionLog +=  "[.] Processing $($Tenants.Count) Tenant(s)"

		foreach ($TName in $Tenants) {
			try {
				$statuses = @{}
				if ($HostPoolName -and $HostPoolName -ne '') {
					Write-Verbose "[.] Getting specific host pool status"
					$SessionLog += "[.] Getting specific host pool status"
					$status = Get-WvdStatus -TenantName $TName -HostPoolName $HostPoolName -TenantCreds $TenantCreds -TenantServicePrincipal:$TenantServicePrincipal -AzureCreds $AzureCreds -AzureServicePrincipal:$AzureServicePrincipal -SubscriptionId $SubscriptionId -DeploymentUrl $DeploymentUrl -AADTid $AADTid
					$statuses[$TName] = @{}
					$statuses[$TName][$HostPoolName] = @()
					$statuses[$TName][$HostPoolName] = $status
				} else {
					Write-Verbose "[.] Logging into RDS"
					$SessionLog += "[.] Logging into RDS"

					if ($TenantServicePrincipal) {
						try {
							$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ServicePrincipal -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
							if ($err) {
								throw $err;
							}
						} catch {
							if (!$UnsetTenantCreds) {
								$global:TenantCredentials = $null
								Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
								$TenantCreds = $global:TenantCredentials
							
								if (!$TenantCreds) {
									throw "No tenant creds, no work, after trying to reset credentials due to a signin error. check your credentials/cred files. related (probably useless) error message: $_"
								}
								$UnsetTenantCreds = $true
								try {
									$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ServicePrincipal -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
									if ($err) {
										throw $err;
									}
								} catch {
									throw "An error occurred on RDS Signin after reimporting credentials: $_"
								}
							} else {
								throw "An error occurred on RDS Signin: $_"
							}
						}
					} else {
						try {
							$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
							if ($err) {
								throw $err;
							}
						} catch {
							if (!$UnsetTenantCreds) {
								$global:TenantCredentials = $null
								Get-Creds -AzureFile $AzureCredentialFilePath -TenantFile $TenantCredentialFilePath -AzurePlainText $AzurePlainText -TenantPlainText $TenantPlainText
								$TenantCreds = $global:TenantCredentials
							
								if (!$TenantCreds) {
									throw "No tenant creds, no work, after trying to reset credentials due to a signin error. check your credentials/cred files. related (probably useless) error message: $_"
								}
								$UnsetTenantCreds = $true
								try {
									$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
									if ($err) {
										throw $err;
									}
								} catch {
									throw "An error occurred on RDS Signin after reimporting credentials: $_"
								}
							} else {
								throw "An error occurred on RDS Signin: $_"
							}
						}
					}
					if (!$RdsContext -or $err) {
						throw "Error on RDS Signin: $err"
					}

					Write-Verbose "[.] Logged into RDS"
					$SessionLog += "[.] Logged into RDS"
					Write-Verbose "[.] Getting all host pool statuses"
					$SessionLog += "[.] Getting all host pool statuses"
					(Get-RdsHostPool $TName).HostPoolName | sort-object -unique | %{ $hn = $_;
						$status = Get-WvdStatus -TenantName $TName -HostPoolName $hn -TenantCreds $TenantCreds -TenantServicePrincipal:$TenantServicePrincipal -AzureCreds $AzureCreds -AzureServicePrincipal:$AzureServicePrincipal -SubscriptionId $SubscriptionId -DeploymentUrl $DeploymentUrl -AADTid $AADTid; 
					$statuses[$TName] = @{}
					$statuses[$TName][$hn] = @()
					$statuses[$TName][$hn] = $status
					}
				}

				Write-Verbose "[.] Got host pool status(es). Beginning Processing."
				$SessionLog += "[.] Got host pool status(es). Beginning Processing."

				foreach ($tenant in $statuses.GetEnumerator()) {
					foreach ($hostpool in $tenant.Value.GetEnumerator()) {
						Write-Host "[.] Checking $($tenant.Name)/$($hostpool.Name)"
						$SessionLog += "[.] Checking $($tenant.Name)/$($hostpool.Name)"
						$statusstring = HostPoolStatus-ToString $hostpool
						Write-Verbose "[.] Full Host Pool Status:"
						Write-Verbose "$statusstring"
						$SessionLog += "[.] Full Host Pool Status:"
						$SessionLog += "$statusstring"
						$ScaleOut = $false
						$ScaleIn = $false
						$AvailCpus = 0;

						$ExcludedSystems = @()

						$CurrentSessions = 0;
						$ActiveSessions = 0;
						
						$hostpool.Value | where {$_.status -eq 'Available' -and $_.'Power State' -eq 'VM running' -and $ExcludeTag -notin $_.Tags.Keys -and $_.AllowNewSession -eq $true} | %{ $AvailCpus += $_.'cpu count'; }
						$NotAvailableForNewSessions = @($hostpool.Value | where {$_.AllowNewSession -ne $true})
						
						$ExcludedSystems = @($hostpool.Value | where {$ExcludeTag -in $_.Tags.Keys})
						$hostpool.Value | %{ $CurrentSessions += $_.'Total Sessions'; }
						$hostpool.Value | %{ $ActiveSessions += $_.'Active Sessions'; }
						Write-Verbose "[.] $AvailCpus CPUs Available"
						$SessionLog += "[.] $AvailCpus CPUs Available"
						Write-Verbose "[.] $CurrentSessions Total Current Sessions"
						$SessionLog += "[.] $CurrentSessions Total Current Sessions"
						Write-Verbose "[.] $ActiveSessions Total Active Sessions"
						$SessionLog += "[.] $ActiveSessions Total Active Sessions"
						
						if ($ExcludedSystems.Count -eq 0) {
							Write-Verbose "[.] $($ExcludedSystems.Count) Systems Excluded by tag $ExcludeTag"
							$SessionLog += "[.] $($ExcludedSystems.Count) Systems Excluded by tag $ExcludeTag"
						} elseif ($ExcludedSystems.Count -eq 1) {
							Write-Verbose "[.] $($ExcludedSystems.Count) System Excluded by tag $ExcludeTag ($($ExcludedSystems.'Session Host Name'))"
							$SessionLog += "[.] $($ExcludedSystems.Count) System Excluded by tag $ExcludeTag ($($ExcludedSystems.'Session Host Name'))"
						} elseif ($ExcludedSystems.Count -gt 1) {
							Write-Verbose "[.] $($ExcludedSystems.Count) Systems Excluded by tag $ExcludeTag ($($ExcludedSystems.'Session Host Name' -join ', '))"
							$SessionLog += "[.] $($ExcludedSystems.Count) Systems Excluded by tag $ExcludeTag ($($ExcludedSystems.'Session Host Name' -join ', '))"
						} else {
							Write-Verbose "[!] System exclusion count is less than 0. Please up your hacking game. Skipping this tenant due to shenanigans."
							$SessionLog += "[!] System exclusion count is less than 0. Please up your hacking game. Skipping this tenant due to shenanigans."
							continue;
						}
						
						if ($NotAvailableForNewSessions.Count -eq 0) {
							Write-Verbose "[.] $($NotAvailableForNewSessions.Count) Systems Marked as Not Available for New Sessions"
							$SessionLog += "[.] $($NotAvailableForNewSessions.Count) Systems Marked as Not Available for New Sessions"
						} elseif ($NotAvailableForNewSessions.Count -eq 1) {
							Write-Verbose "[.] $($NotAvailableForNewSessions.Count) System Marked as Not Available for New Sessions"
							$SessionLog += "[.] $($NotAvailableForNewSessions.Count) System Marked as Not Available for New Sessions"
						} elseif ($NotAvailableForNewSessions.Count -gt 1) {
							Write-Verbose "[.] $($NotAvailableForNewSessions.Count) Systems Marked as Not Available for New Sessions ($($NotAvailableForNewSessions.'Session Host Name' -join ', '))"
							$SessionLog += "[.] $($NotAvailableForNewSessions.Count) Systems Marked as Not Available for New Sessions ($($NotAvailableForNewSessions.'Session Host Name' -join ', '))"
						} else {
							Write-Verbose "[!] System Marked as Not Available for New Sessions count is less than 0. Please up your hacking game. Skipping this tenant due to shenanigans."
							$SessionLog += "[!] System Marked as Not Available for New Sessions count is less than 0. Please up your hacking game. Skipping this tenant due to shenanigans."
							continue;
						}

						Write-Verbose "[.] Determining Scale Out Necessity"
						$SessionLog += "[.] Determining Scale Out Necessity"
						
						

						if ($ScaleOutMethod.tolower() -eq 'percpu') {
							$MaxSessionThreshold = [math]::Floor($ScaleOutThreshold * $AvailCpus)
						} elseif ($ScaleOutMethod.tolower() -eq 'offset') {
							$MaxSessionThreshold = [math]::Floor($AvailCpus - $ScaleOutThreshold)
						} else {
							throw "Unknown Scaling Method specified. Use 'PerCpu' (as in max sessions per CPU) or 'Offset' (as in n sessions less than the available CPUs)"
						}
						Write-Verbose "[.] $ActiveSessions Active Sessions / $MaxSessionThreshold Max Sessions"
						$SessionLog += "[.] $ActiveSessions Active Sessions / $MaxSessionThreshold Max Sessions"
						Write-Verbose "[.] $CurrentSessions Total Sessions / $MaxSessionThreshold Max Sessions"
						$SessionLog += "[.] $CurrentSessions Total Sessions / $MaxSessionThreshold Max Sessions"

						if ($RecentConns -and $RecentConns.Count -ge 1) {
							$HpRecentConns = $RecentConns | where {$_.SessionHostPoolName_s.ToLower() -eq $hostpool.Name.tolower()} # UNTESTED 20200813
							Write-Verbose "[.] $($HpRecentConns.Count) Recent Connections in this Host Pool"
						} else {
							$HpRecentConns = $RecentConns
						}

						if ($MonitorAfterHours -eq $true -and $OutsideWorkingHours -eq $true -and $MaxSessionThreshold -lt 1 -and $HpRecentConns.Count -ge 1) { # outside working hours, monitoring after hours connection attempts, no cpus available, there was at least one connection attempt
							Write-Host "[i] After hours and a connection attempt was recently made, determining systems to scale out..."
							$SessionLog += "[i] After hours and a connection attempt was recently made, determining systems to scale out..."
							$ScaleOut = $true
						} elseif ($CurrentSessions -lt 1 -and $OutsideWorkingHours) {
							Write-Host "[.] No current sessions and outside of normal working hours. Scale Out not recommended."
							$SessionLog += "[.] No current sessions and outside of normal working hours. Scale Out not recommended."
						} elseif ($ActiveSessions -ge $MaxSessionThreshold) {
							Write-Host "[i] Recommend Scale Out, determining systems to scale out..."
							$SessionLog += "[i] Recommend Scale Out, determining systems to scale out..."
							$ScaleOut = $true
						} elseif ($MaxSessionThreshold -lt 1 -and -not $OutsideWorkingHours) { # inside working hours and no cpus available
							Write-Host "[i] Recommend Scale Out at least one system during working hours, determining systems to scale out..."
							$SessionLog += "[i] Recommend Scale Out at least one system during working hours, determining systems to scale out..."
							$ScaleOut = $true
						} elseif ($CurrentSessions -ge $MaxSessionThreshold -and -not ($ScaleOnTotalSessions -eq $true)) {
							Write-Verbose "[w] Warning - Not Recommending Scale Out, but may need to soon"
							$SessionLog += "[w] Warning - Not Recommending Scale Out, but may need to soon"
						} elseif ($CurrentSessions -ge $MaxSessionThreshold -and ($ScaleOnTotalSessions -eq $true)) {
							Write-Verbose "[i] Recommending Scale Out based on total sessions"
							$SessionLog += "[i] Recommending Scale Out based on total sessions"
							$ScaleOut = $true
						} else {
							Write-Verbose "[.] All Good!"
							$SessionLog += "[.] All Good!"
						}

						Write-Verbose "[.] Determining Scale In Necessity"
						$SessionLog += "[.] Determining Scale In Necessity"

						if ($ScaleInMethod.tolower() -eq 'percpu') {
							$MinSessionThreshold = [math]::Ceiling($ScaleInThreshold * $AvailCpus)
						} elseif ($ScaleInMethod.tolower() -eq 'offset') {
							$MinSessionThreshold = [math]::Ceiling($AvailCpus - $ScaleInThreshold)
						} else {
							throw "Unknown Scaling Method specified. Use 'PerCpu' (as in max sessions per CPU) or 'Offset' (as in n sessions less than the available CPUs)"
						}

						Write-Verbose "[.] $ActiveSessions Active Sessions / $MinSessionThreshold Min Sessions"
						$SessionLog += "[.] $ActiveSessions Active Sessions / $MinSessionThreshold Min Sessions"
						Write-Verbose "[.] $CurrentSessions Total Sessions / $MinSessionThreshold Min Sessions"
						$SessionLog += "[.] $CurrentSessions Total Sessions / $MinSessionThreshold Min Sessions"

						if (!$ScaleOut -and $HpRecentConns.Count -gt 0) {
							Write-Verbose "[.] After Hours, systems still running, and recent attempted sessions - don't scale in"
							$SessionLog += "[.] After Hours, systems still running, and recent attempted sessions - don't scale in"
						} elseif ($OutsideWorkingHours -and $MinSessionThreshold -gt 0) {
							Write-Verbose "[.] After Hours and systems still running - scale in!"
							$SessionLog += "[.] After Hours and systems still running - scale in!"
							$ScaleIn = $true
						} elseif ($CurrentSessions -lt 1 -and $MinSessionThreshold -lt 1) {
							Write-Verbose "[.] No sessions and no CPUs, no need to scale in"
							$SessionLog += "[.] No sessions and no CPUs, no need to scale in"
						} elseif ($ActiveSessions -le $MinSessionThreshold) {
							Write-Host "[i] Recommend Scale In, determining systems to scale in..."
							$SessionLog += "[i] Recommend Scale In, determining systems to scale in..."
							$ScaleIn = $true
						} elseif ($CurrentSessions -le $MinSessionThreshold) {
							Write-Verbose "[w] Warning - Not Recommending Scale In, but may need to soon"
							$SessionLog += "[w] Warning - Not Recommending Scale In, but may need to soon"
						} else {
							Write-Verbose "[.] All Good!"
							$SessionLog += "[.] All Good!"
						}

						if ($ScaleOut -and $ScaleIn) {
							if ($OutsideWorkingHours) {
								Write-Verbose "[i] After Hours, no sessions"
								$SessionLog += "[i] After Hours, no sessions"
								$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] After Hours, no sessions") + $SessionLog
								continue
							}
							Write-Verbose "[e] Determined to scale out and scale in. Something is a miss..."
							$SessionLog += "[e] Determined to scale out and scale in. Something is a miss..."
							continue
						} elseif (!$ScaleOut -and !$ScaleIn) {
							Write-Host "[.] Looking good, you're 'Right Sized' (per your specifications, not mine)"
							$SessionLog += "[.] Looking good, you're 'Right Sized' (per your specifications, not mine)"
							$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Looking good, you're 'Right Sized' (per your specifications, not mine)") + $SessionLog
							continue
						} elseif ($ScaleOut) {

							### Scale Out ###

							$AvailForScaleOut = $hostpool.Value | where {$_.status -eq 'Unavailable' -and $_.'Power State' -eq 'VM deallocated' -and $_.'Session Host Name' -notin @($ExcludedSystems.'Session Host Name')} # Optimal situation. assumes hostnames are unique.
							$CpusAvailForScaleOut = 0; $AvailForScaleOut | %{ $CpusAvailForScaleOut += $_.'cpu count'; }
							$Buffer = 0; # Buffer is the minimum number of CPUs requested to scale out to
							if ($ScaleOutBufferType.tolower() -eq 'sessions') {
								$Buffer = [math]::Ceiling($ScaleOutBuffer * $ScaleOutThreshold)
							} elseif ($ScaleOutBufferType.tolower() -eq 'cpus') {
								$Buffer = $ScaleOutBuffer
							} elseif ($ScaleOutBufferType.tolower() -eq 'percent') {
								throw "(forthcoming) Unknown Scale buffer type. Use 'Sessions' (as in min number of sessions to support) or 'CPUs' (as in min number of CPUs to add)"
							} else {
								throw "Unknown Scale buffer type. Use 'Sessions' (as in min number of sessions to support) or 'CPUs' (as in min number of CPUs to add)"
							}

							Write-Verbose "[.] Searching for $Buffer more CPUs..."
							$SessionLog += "[.] Searching for $Buffer more CPUs..."

							Write-Verbose "[.] $CpusAvailForScaleOut more CPUs are available in $(@($AvailForScaleOut).count) VMs"
							$SessionLog += "[.] $CpusAvailForScaleOut more CPUs are available in $(@($AvailForScaleOut).count) VMs"

							if ($CpusAvailForScaleOut -lt 1) {
								Write-Host "[w] Warning - I recommend scaling out, but you have no further systems to engage. I need more power!"
								$SessionLog += "[w] Warning - I recommend scaling out, but you have no further systems to engage. I need more power!"
								$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)]  [$TenantName | $HostPoolName]I recommend scaling out, but you have no further systems to engage. I need more power!") + $SessionLog
								continue
							} elseif ($CpusAvailForScaleOut -lt $Buffer) {
								Write-Host "[w] Warning - you requested to scale out by $Buffer CPUs, but I only appear to have $CpusAvailForScaleOut CPUs available"
								$SessionLog += "[w] Warning - you requested to scale out by $Buffer CPUs, but I only appear to have $CpusAvailForScaleOut CPUs available"
								Write-Host "[i] I'll scale out what I can"
								$SessionLog += "[i] I'll scale out what I can"
							}
							$TargetCpus = $Buffer
							$VmsToStart = @()
							while ($TargetCpus -gt 0 -and ($VmsToStart.count -lt @($AvailForScaleOut).count)) {
								$VmsToStart += ($AvailForScaleOut | sort-object 'CPU Count','Session Host Name')[0] # Start with the smallest. this may be optimizable in cases when one large available system could be started to satisfy the requested cpu count instead of two smaller systems. This also does not adjust for special vm sizes, such as memory optimized, GPU, burstable, etc.
								$TargetCpus -= $VmsToStart[-1].'CPU Count'
							}

							$jobs = @()
							if (!$Simulation) {
								Write-Verbose "[i] Starting $($VmsToStart.Count) VMs ($(@($VmsToStart.'Session Host Name') -join ', '))"
								$SessionLog += "[i] Starting $($VmsToStart.Count) VMs ($(@($VmsToStart.'Session Host Name') -join ', '))"
								$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Starting $($VmsToStart.Count) VMs ($(@($VmsToStart.'Session Host Name') -join ', '))") + $SessionLog
								$VmsToStart | %{ $job = Start-AzVm -ResourceGroupName $_.'Resource Group Name' -Name $(($_.'Session Host Name' -split '\.',2)[0]) -AsJob ; $jobs += $job ; }
								if (!$NoWait) {
									Write-Verbose "[.] Waiting for jobs to complete"
									$SessionLog += "[.] Waiting for jobs to complete"
									while ($((Get-Job | where {$_.Id -in $jobs.Id -and $_.State -eq 'Running'}).Count) -gt 0) {
										sleep 1
									}
									if ($(Get-Job | where {$_.Id -in $jobs.id -and $_.state -ne 'Completed'}).count -gt 0) {
										Write-Host "[e] One or more jobs requires your attention. I give up here:"
										$SessionLog += "[e] One or more jobs requires your attention. I give up here:"
										$Results = Get-Job
										$Results
										$SessionLog += $Results
										continue
									}
									$Results = Get-Job | where {$_.Id -in $jobs.Id} | receive-job -AutoRemoveJob -wait
									$SessionLog += $Results
								}
							} else {
								Write-Host "[i] Recommend Scaling Out by $($VmsToStart.Count) VMs ($(@($VmsToStart.'Session Host Name') -join ', '))"
								$SessionLog += "[i] Recommend Scaling Out by $($VmsToStart.Count) VMs ($(@($VmsToStart.'Session Host Name') -join ', '))"
								$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Recommend Scaling Out by $($VmsToStart.Count) VMs ($(@($VmsToStart.'Session Host Name') -join ', '))") + $SessionLog
							}

							Write-Host "[.] Success"
							$SessionLog += "[.] Success"

							continue
							
						} elseif ($ScaleIn) {

							### Scale In ###

							$ready = $false

							$AvailForScaleIn = $hostpool.Value | where {$_.'Power State' -eq 'VM running' -and $_.'Total Sessions' -eq 0 -and $_.'Session Host Name' -notin @($ExcludedSystems.'Session Host Name')} # Optimal situation

							$Available = $hostpool.Value | where {$_.'Power State' -eq 'VM running' -and $_.status -eq 'Available'}

							if ($AllowCompleteShutdown -eq $true -and @($AvailForScaleIn).Count -gt 0 -and $OutsideWorkingHours) { # there are hosts that are not in use outside of normal working hours; shut them down. 
								Write-Verbose "[.] $(@($AvailForScaleIn).Count) systems are not in use, and it is outside of normal working hours - shut 'em down"
								$SessionLog += "[.] $(@($AvailForScaleIn).Count) systems are not in use, and it is outside of normal working hours - shut 'em down"
								$NotAvailForScaleIn = $hostpool.Value | where {$_.'Power State' -eq 'VM running' -and $_.'Total Sessions' -gt 0 -and $_.'Session Host Name' -notin @($ExcludedSystems.'Session Host Name')}
								if (@($NotAvailForScaleIn).Count -gt 0) {
									Write-Verbose "[.] $(@($NotAvailForScaleIn).Count) non-excluded systems are still in use, and it is outside of normal working hours - leaving them alone for now."
									$SessionLog += "[.] $(@($NotAvailForScaleIn).Count) non-excluded systems are still in use, and it is outside of normal working hours - leaving them alone for now."
								} else {
									$ready = $true; # shortcut the next loop
								}
							}
							if ($ready -eq $true -or (@($AvailForScaleIn).Count -gt 0 -and @($Available).Count -gt $MinHostCount)) { # has something to scale in, and user is ok with it. does not account for unavailable running systems
								while (!$ready -and @($AvailForScaleIn).count -ge 1) {
									$CpusToRemove = 0; $AvailForScaleIn | %{ $CpusToRemove += $_.'cpu count'; };

									if ($ScaleOutMethod.tolower() -eq 'percpu') {
										$MaxSessionThreshold = [math]::Floor($ScaleOutThreshold * ($AvailCpus - $CpusToRemove))
									} elseif ($ScaleOutMethod.tolower() -eq 'offset') {
										$MaxSessionThreshold = [math]::Floor(($AvailCpus - $CpusToRemove) - $ScaleOutThreshold)
									} else {
										throw "Unknown Scaling Method specified. Use 'PerCpu' (as in max sessions per CPU) or 'Offset' (as in n sessions less than the available CPUs)"
									}

									Write-Verbose "[.] Proposal: Scale In by $(@($AvailForScaleIn).Count) systems"
									$SessionLog += "[.] Proposal: Scale In by $(@($AvailForScaleIn).Count) systems"
									Write-Verbose "[.] $ActiveSessions Active Sessions / $MaxSessionThreshold Proposed Max Sessions"
									$SessionLog += "[.] $ActiveSessions Active Sessions / $MaxSessionThreshold Proposed Max Sessions"
									Write-Verbose "[.] $CurrentSessions Total Sessions / $MaxSessionThreshold Proposed Max Sessions"
									$SessionLog += "[.] $CurrentSessions Total Sessions / $MaxSessionThreshold Proposed Max Sessions"

									if ($ActiveSessions -ge $MaxSessionThreshold) {
										Write-Verbose "[.] Current Active Sessions meet or exceed session threshold after proposed cut. Reducing cut level."
										$SessionLog += "[.] Current Active Sessions meet or exceed session threshold after proposed cut. Reducing cut level."
										if (@($AvailForScaleIn).count -gt 1) {
											$AvailForScaleIn = $AvailForScaleIn[0..($AvailForScaleIn.Length-2)]
										} else {
											break
										}
									} elseif ($CurrentSessions -ge $MaxSessionThreshold) {
										Write-Verbose "[.] Current Sessions (including disconnected sessions) meet or exceed proposed cut. Reducing cut level."
										$SessionLog += "[.] Current Sessions (including disconnected sessions) meet or exceed proposed cut. Reducing cut level."
										if (@($AvailForScaleIn).count -gt 1) {
											$AvailForScaleIn = $AvailForScaleIn[0..($AvailForScaleIn.Length-2)]
										} else {
											break
										}
									} else {
										Write-Verbose "[.] All Good!"
										$SessionLog += "[.] All Good!"
										$ready = $true
									}
								}

								if (@($AvailForScaleIn).count -lt 1 -or !$ready) {
									Write-Host "[i] Insufficient Resources will remain if we scale in, so forgoing scaling at this time"
									$SessionLog += "[i] Insufficient Resources will remain if we scale in, so forgoing scaling at this time"
									$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Insufficient Resources will remain if we scale in, so forgoing scaling at this time") + $SessionLog
									continue
								}

								$jobs = @()

								if (!$Simulation) {
									Write-Verbose "[i] Stopping $(@($AvailForScaleIn).count) VMs ($(@($AvailForScaleIn).'Session Host Name' -join ', '))"
									$SessionLog += "[i] Stopping $(@($AvailForScaleIn).count) VMs ($(@($AvailForScaleIn).'Session Host Name' -join ', '))"
									$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Stopping $(@($AvailForScaleIn).count) VMs ($(@($AvailForScaleIn).'Session Host Name' -join ', '))") + $SessionLog
									$AvailForScaleIn | %{ $job = Stop-AzVm -ResourceGroupName $_.'Resource Group Name' -Name $(($_.'Session Host Name' -split '\.',2)[0]) -AsJob -Force -NoWait ; $jobs += $job ; }
									if (!$NoWait) {
										Write-Verbose "[.] Waiting for jobs to complete"
										$SessionLog += "[.] Waiting for jobs to complete"
										while ($((Get-Job | where {$_.Id -in $jobs.Id -and $_.State -eq 'Running'}).Count) -gt 0) {
											sleep 1
										}
										if ($(Get-Job | where {$_.Id -in $jobs.id -and $_.state -ne 'Completed'}).count -gt 0) {
											Write-Host "[e] One or more jobs requires your attention. I give up here:"
											$SessionLog += "[e] One or more jobs requires your attention. I give up here:"
											$Results = Get-Job
											$Results
											$SessionLog += $Results
											continue
										}

										$Results = Get-Job | where {$_.Id -in $jobs.Id} | receive-job -AutoRemoveJob -wait
										$SessionLog += $Results
									}
								} else {
									Write-Host "[i] Recommend Scaling In by $(@($AvailForScaleIn).count) VMs ($(@($AvailForScaleIn).'Session Host Name' -join ', '))"
									$SessionLog += "[i] Recommend Scaling In by $(@($AvailForScaleIn).count) VMs ($(@($AvailForScaleIn).'Session Host Name' -join ', '))"
									$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Recommend Scaling In by $(@($AvailForScaleIn).count) VMs ($(@($AvailForScaleIn).'Session Host Name' -join ', '))") + $SessionLog
								}

							} elseif (@($Available).Count -le $MinHostCount) { # Already at min hosts
								Write-Host "[i] Already at minimum user-specified VM threshold ($MinHostCount)"
								$SessionLog += "[i] Already at minimum user-specified VM threshold ($MinHostCount)"
								$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Already at minimum user-specified VM threshold ($MinHostCount)") + $SessionLog
								continue
							} elseif (@($AvailForScaleIn).Count -lt 1) { # nothing to scale in
								Write-Host "[i] Nothing is available to scale in. This may be the case if there are single user sessions across numerous VMs, even disconnected user sessions. Once they're logged off (default 60min), I can scale in those VMs"
								$SessionLog += "[i] Nothing is available to scale in. This may be the case if there are single user sessions across numerous VMs, even disconnected user sessions. Once they're logged off (default 60min), I can scale in those VMs"
								$SessionLog = @("tl;dr: [$($tenant.Name)/$($hostpool.Name)] Nothing is available to scale in. This may be the case if there are single user sessions across numerous VMs, even disconnected user sessions. Once they're logged off (default 60min), I can scale in those VMs") + $SessionLog
								continue
							} else { # not sure whats up here
								Write-Host "[e] Not sure how you got here... Congratulations, you've been crowned Hacker Supreme! (Blue Falcon)"
								$SessionLog += "[e] Not sure how you got here... Congratulations, you've been crowned Hacker Supreme! (Blue Falcon)"
								continue
							}

							Write-Host "[.] Success"
							$SessionLog += "[.] Success"

							continue
							
						} else {
							Write-Host "[e] Not sure how you got here... Congratulations, you've been crowned Hacker Supreme! (Green Serpent)"
							$SessionLog += "[e] Not sure how you got here... Congratulations, you've been crowned Hacker Supreme! (Green Serpent)"
							continue
						}
					}
				}
			} catch {
				Write-Host "[e] An Error occurred while processing Tenant $Tname : $_"
				$SessionLog += "[e] An Error occurred while processing Tenant $Tname : $_"
				continue		
			}
		}
	} catch {
		Write-Host "[!] An Error occurred: $_"
		$SessionLog += "[!] An Error occurred: $_"
	} finally {
		if ($UnsetTenantCreds) {
			Write-Verbose "[.] Removing captured tenant credentials"
			$SessionLog += "[.] Removing captured tenant credentials"
			$Global:TenantCreds = $null
		}
		if ($UnsetAzureCreds) {
			Write-Verbose "[.] Removing captured azure credentials"
			$SessionLog += "[.] Removing captured azure credentials"
			$Global:AzureCreds = $null
		}
		Write-Verbose "[.] Done!"

		$SessionEndDate = Get-Date
		Write-Verbose "$SessionEndDate"
		$SessionLog += "$SessionEndDate"
		$SessionDuration = New-TimeSpan -Start $SessionStartDate -End $SessionEndDate
		Write-Verbose "[.] Session Duration: $SessionDuration"
		$SessionLog += "[.] Session Duration: $SessionDuration"
		$SessionLog += "Session End"

		Write-Log "$($SessionLog -join "`n")" -eventid $EventId
		if ($WriteToSyslog) {
			@($SyslogServer) | %{
				try {
					# write to syslog servers
					Write-Verbose "Write to syslog feature forthcoming!"
				} catch {
					# handle error
					Write-Verbose "Write to syslog feature forthcoming!"
				}
			}
		}
	}
}

# TODO: Convert to take all object attributes
function Get-WvdStatus ($TenantName, $HostPoolName, $TenantCreds = $Global:TenantCredentials, [Switch]$TenantServicePrincipal = $Global:DefaultTenantServicePrincipal, $AzureCreds = $Global:AzureCredentials, [Switch]$AzureServicePrincipal = $Global:DefaultAzureServicePrincipal, $SubscriptionId = $global:DefaultSubscriptionId, $DeploymentUrl = $Global:DefaultDeploymentUrl, $AADTid = $Global:DefaultTenantId) {
	if (!$TenantName -or !$HostPoolName -or $TenantName -eq '' -or $HostPoolName -eq'') {
		throw "Pass tenant name and host pool name"
	}

	$ret = @()

	Write-Verbose "getting host pool status"
	if ($TenantServicePrincipal) {
		$sessionhosts = Get-HostPoolStatus -TenantName $TenantName -HostPoolName $HostPoolName -TenantCreds $TenantCreds -ServicePrincipal -DeploymentUrl $DeploymentUrl -AADTid $AADTid;
	} else {
		$sessionhosts = Get-HostPoolStatus -TenantName $TenantName -HostPoolName $HostPoolName -TenantCreds $TenantCreds -DeploymentUrl $DeploymentUrl -AADTid $AADTid;
	}

	Write-Verbose "getting host pool status fin"
	Write-Verbose "processing host pool status"
	$sessionhosts | %{
		$sh = New-Object -TypeName psobject
		$sh | Add-Member -MemberType NoteProperty -Name 'Tenant Name' -Value $TenantName
		$sh | Add-Member -MemberType NoteProperty -Name 'Host Pool Name' -Value $HostPoolName
		$sh | Add-Member -MemberType NoteProperty -Name 'Session Host Name' -Value $_.SessionHostName
		$sh | Add-Member -MemberType NoteProperty -Name 'Total Sessions' -Value $_.'Total Sessions'
		$sh | Add-Member -MemberType NoteProperty -Name 'Active Sessions' -Value $_.'Active Sessions'
		$sh | Add-Member -MemberType NoteProperty -Name Status -Value $_.Status
		$sh | Add-Member -MemberType NoteProperty -Name AllowNewSession -Value $_.AllowNewSession -Force
		try {
			Write-Verbose "enriching $($_.SessionHostName)"
			if ($AzureServicePrincipal) {
				$status = Get-VmStatus $_.SessionHostName -AzureCreds $AzureCreds -AzureServicePrincipal -SubscriptionId $SubscriptionId -AADTid $AADTid
				$vmdetails = Get-VmDetails $_.SessionHostName -AzureCreds $AzureCreds -AzureServicePrincipal -SubscriptionId $SubscriptionId -AADTid $AADTid
			} else {
				$status = Get-VmStatus $_.SessionHostName -AzureCreds $AzureCreds -SubscriptionId $SubscriptionId -AADTid $AADTid
				$vmdetails = Get-VmDetails $_.SessionHostName -AzureCreds $AzureCreds -SubscriptionId $SubscriptionId -AADTid $AADTid
			}
			Write-Verbose "finalizing"
			$sh | Add-Member -MemberType NoteProperty -Name 'Power State' -Value $status.'Power State'
			$sh | Add-Member -MemberType NoteProperty -Name 'Resource Group Name' -Value $status.'Resource Group Name'
			$sh | Add-Member -MemberType NoteProperty -Name 'CPU Count' -Value $vmdetails.CPUs
			$sh | Add-Member -MemberType NoteProperty -Name 'Tags' -Value $vmdetails.Tags
		} catch {
			$sh | Add-Member -MemberType NoteProperty -Name 'Power State' -Value ''
			$sh | Add-Member -MemberType NoteProperty -Name 'Resource Group Name' -Value ''
			$sh | Add-Member -MemberType NoteProperty -Name 'CPU Count' -Value ''
			$sh | Add-Member -MemberType NoteProperty -Name 'Tags' -Value @{}
		}
		$ret += $sh
	}

	return $ret
}

function Get-HostPoolStatus ($TenantName, $HostPoolName, $TenantCreds = $global:TenantCredentials, [Switch]$ServicePrincipal = $false, $DeploymentUrl = $global:DefaultDeploymentUrl, $AADTid = $global:DefaultTenantId) {
	if (!$TenantName -or !$HostPoolName -or $TenantName -eq '' -or $HostPoolName -eq'') {
		throw "Pass tenant name and host pool name"
	}
	if (!$TenantCreds) {
		$global:TenantCredentials = $null
		Get-Creds
		$TenantCreds = $global:TenantCredentials	
	}
	if ($ServicePrincipal) {
		try {
			$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ServicePrincipal -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
			if ($err) {
				throw $err;
			}
		} catch {
			if (!$UnsetTenantCreds) {
				$global:TenantCredentials = $null
				Get-Creds
				$TenantCreds = $global:TenantCredentials
			
				if (!$TenantCreds) {
					throw "No tenant creds, no work, after trying to reset credentials due to a signin error. check your credentials/cred files. related (probably useless) error message: $_"
				}
				$UnsetTenantCreds = $true
				try {
					$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ServicePrincipal -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
					if ($err) {
						throw $err;
					}
				} catch {
					throw "An error occurred on RDS Signin after reimporting credentials: $_"
				}
			} else {
				throw "An error occurred on RDS Signin: $_"
			}
		}
	} else {
		try {
			$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
			if ($err) {
				throw $err;
			}
		} catch {
			if (!$UnsetTenantCreds) {
				$global:TenantCredentials = $null
				Get-Creds
				$TenantCreds = $global:TenantCredentials
			
				if (!$TenantCreds) {
					throw "No tenant creds, no work, after trying to reset credentials due to a signin error. check your credentials/cred files. related (probably useless) error message: $_"
				}
				$UnsetTenantCreds = $true
				try {
					$RdsContext = Add-RdsAccount -DeploymentUrl $DeploymentUrl -Credential $TenantCreds -AadTenantId $AADTid -ErrorAction SilentlyContinue -ErrorVariable err -WarningAction SilentlyContinue
					if ($err) {
						throw $err;
					}
				} catch {
					throw "An error occurred on RDS Signin after reimporting credentials: $_"
				}
			} else {
				throw "An error occurred on RDS Signin: $_"
			}
		}
	}
	if (!$RdsContext -or $err) {
		throw "Error on RDS Signin: $err"
	}
	$sessionhosts = @();
	try {
		$tsessionhosts = Get-RdsSessionHost $TenantName $HostPoolName | select SessionHostName,Sessions,Status,AllowNewSession;
		$as = @()
		$tsessionhosts | %{
			$obj = New-Object -TypeName psobject; 
			$h = $_; 
			$obj | add-member -MemberType NoteProperty -name SessionHostName -value $h.sessionhostname; 
			$obj | add-member -MemberType NoteProperty -name 'Total Sessions' -value $h.sessions; 
			$obj | add-member -MemberType NoteProperty -name 'Status' -value $h.status; 
			$obj | add-member -MemberType NoteProperty -name 'AllowNewSession' -value $h.AllowNewSession; 
			$obj | add-member -MemberType NoteProperty -name 'Active Sessions' -value $(Get-RdsUserSession $TenantName $HostPoolName | where {$_.sessionhostname -eq $h.sessionhostname -and $_.sessionstate -eq 'Active'}).Count; 
			$sessionhosts += $obj;
		}
	} catch {
		$sessionhosts = @();
	}
	return $sessionhosts;
}

function Get-VmDetails ($VmName, $AzureCreds = $AzureCredentials, [Switch]$AzureServicePrincipal = $false, $SubscriptionId = $COSubscriptionId, $AADTid = $DefaultTenantId) {
	if (!$AzureCreds) {
		$global:AzureCredentials = $null
		Get-Creds
		$AzureCreds = $global:AzureCredentials	
	}
	if (!(Login-Az -Credentials $AzureCreds -TenantId $AADTid -SubscriptionId $SubscriptionId -ServicePrincipal:$AzureServicePrincipal)) {
		throw 'Unable to login to AZ'
	}
	$vm = get-azvm | where {$_.name -eq $(($VmName -split '\.',2)[0])}
	$obj = New-Object  -TypeName psobject
	$obj | Add-Member -MemberType NoteProperty -Name Name -Value $vm.Name
	$obj | Add-Member -MemberType NoteProperty -Name Tags -Value $vm.Tags
	$obj | Add-Member -MemberType NoteProperty -Name CPUs -Value $(Get-AzVMSize -ResourceGroupName $vm.ResourceGroupName -VMName $vm.Name | where {$_.name -eq $vm.HardwareProfile.VmSize}).numberofcores
	return $obj
}

function Get-VmStatus ($VmName, $AzureCreds = $AzureCredentials, [Switch]$AzureServicePrincipal = $false, $SubscriptionId = $global:DefaultSubscriptionId, $AADTid = $DefaultTenantId) {

	if (!(Login-Az -Credentials $AzureCreds -TenantId $AADTid -SubscriptionId $SubscriptionId -ServicePrincipal:$AzureServicePrincipal)) {
		throw 'Unable to login to AZ'
	}

	$vm = get-azvm -status | where {$_.name -eq $(($VmName -split '\.',2)[0])}
	$obj = New-Object -TypeName psobject
	$obj | Add-Member -MemberType NoteProperty -Name Name -Value $vm.Name
	$obj | Add-Member -MemberType NoteProperty -Name 'Resource Group Name' -Value $vm.ResourceGroupName
	$obj | Add-Member -MemberType NoteProperty -Name 'Provisioning State' -Value $vm.provisioningstate
	$obj | Add-Member -MemberType NoteProperty -Name 'Power State' -Value $vm.powerstate
	return $obj
}

# TODO: accept list
function PsObjectTo-HashTable ($Object) {
	$ret = New-Object System.Collections.HashTable
	try {
		$Object.psobject.properties | %{
			try {
				if ($_.Name -and $_.Value) {
					$ret[$_.Name] = $_.Value
				}
			} catch {}
		}
	} catch {}
	return $ret
}

# Timespan reference: https://en.wikipedia.org/wiki/ISO_8601#Durations
function Get-ConnectionAttempts ($WorkspaceName = $global:DefaultLogAnalyticsWorkspaceName, $LawSubscriptionId = $global:DefaultSubscriptionId, $ResourceGroupName = $global:DefaultLogAnalyticsWorkspaceResourceGroupName, $Query = '', $Timespan = "P0Y0M0DT0H65M", $AzureCreds = $AzureCredentials, [Switch]$AzureServicePrincipal = $false, $AzureSubscriptionId = $COSubscriptionId, $AADTid = $DefaultTenantId) {
	$ret = @{}
	if (!(Login-Az -Credentials $AzureCreds -TenantId $AADTid -SubscriptionId $AzureSubscriptionId -ServicePrincipal:$AzureServicePrincipal)) {
		throw 'Unable to login to AZ'
	}

	if (!$Query -or $Query -eq '') { # default query should return actual app/default connection attempts 
		$Query = @'
WVDActivityV1_CL
| where Method_s <> "Get"
| where Type_s == "Connection"
'@
	}

	if (!$Timespan -or $Timespan -eq '' -or $Timespan -notmatch "^P\d+Y\d+M\d+DT\d+H\d+M$") {
		Write-Verbose "[i] Bad Timespan Specified. Using default value ("P0Y0M0DT0H65M"). See https://en.wikipedia.org/wiki/ISO_8601#Durations for assistance."
		$Timespan = "P0Y0M0DT0H65M"
	}

	$res = Invoke-LogAnalyticsQuery -WorkspaceName $WorkspaceName -SubscriptionId $LawSubscriptionId -ResourceGroup $ResourceGroupName -Query $Query -Timespan $Timespan

	try {
		$ret = $res.Results
	} catch {
		$ret = $null
	}

	return $ret
}

# the following functions were bogarted directly from Eli Shlomo (https://www.eshlomo.us/query-azure-log-analytics-data-with-powershell/), with minor modification (Switch to Az module). Thanks!

<#
    .DESCRIPTION
        Invokes a query against the Log Analtyics Query API.

    .EXAMPLE
        Invoke-LogAnaltyicsQuery -WorkspaceName my-workspace -SubscriptionId 0f991b9d-ab0e-4827-9cc7-984d7319017d -ResourceGroup my-resourcegroup
            -Query "union * | limit 1" -CreateObjectView

    .PARAMETER WorkspaceName
        The name of the Workspace to query against.

    .PARAMETER SubscriptionId
        The ID of the Subscription this Workspace belongs to.

    .PARAMETER ResourceGroup
        The name of the Resource Group this Workspace belongs to.

    .PARAMETER Query
        The query to execute.
    
    .PARAMETER Timespan
        The timespan to execute the query against. This should be an ISO 8601 timespan.

    .PARAMETER IncludeTabularView
        If specified, the raw tabular view from the API will be included in the response.

    .PARAMETER IncludeStatistics
        If specified, query statistics will be included in the response.

    .PARAMETER IncludeRender
        If specified, rendering statistics will be included (useful when querying metrics).

    .PARAMETER ServerTimeout
        Specifies the amount of time (in seconds) for the server to wait while executing the query.

    .PARAMETER Environment
        Internal use only.
#>
function Invoke-LogAnalyticsQuery {
param(
    [string]
    [Parameter(Mandatory=$true)]
    $WorkspaceName,

    [guid]
    [Parameter(Mandatory=$true)]
    $SubscriptionId,

    [string]
    [Parameter(Mandatory=$true)]
    $ResourceGroup,

    [string]
    [Parameter(Mandatory=$true)]
    $Query,

    [string]
    $Timespan,

    [switch]
    $IncludeTabularView,

    [switch]
    $IncludeStatistics,

    [switch]
    $IncludeRender,

    [int]
    $ServerTimeout,

    [string]
    [ValidateSet("", "int", "aimon")]
    $Environment = ""
    )

    $ErrorActionPreference = "Stop"

    $accessToken = GetAccessToken

    $armhost = GetArmHost $environment

    $queryParams = @("api-version=$global:LawApiVersion")

    $queryParamString = [string]::Join("&", $queryParams)

    $uri = BuildUri $armHost $subscriptionId $resourceGroup $workspaceName $queryParamString

    $body = @{
        "query" = $query;
        "timespan" = $Timespan
    } | ConvertTo-Json

    $headers = GetHeaders $accessToken -IncludeStatistics:$IncludeStatistics -IncludeRender:$IncludeRender -ServerTimeout $ServerTimeout

    $response = Invoke-WebRequest -UseBasicParsing -Uri $uri -Body $body -ContentType "application/json" -Headers $headers -Method Post

    if ($response.StatusCode -ne 200 -and $response.StatusCode -ne 204) {
        $statusCode = $response.StatusCode
        $reasonPhrase = $response.StatusDescription
        $message = $response.Content
        throw "Failed to execute query.`nStatus Code: $statusCode`nReason: $reasonPhrase`nMessage: $message"
    }

    $data = $response.Content | ConvertFrom-Json

    $result = New-Object PSObject
    $result | Add-Member -MemberType NoteProperty -Name Response -Value $response

    # In this case, we only need the response member set and we can bail out
    if ($response.StatusCode -eq 204) {
        $result
        return
    }

    $objectView = CreateObjectView $data

    $result | Add-Member -MemberType NoteProperty -Name Results -Value $objectView

    if ($IncludeTabularView) {
        $result | Add-Member -MemberType NoteProperty -Name Tables -Value $data.tables
    }

    if ($IncludeStatistics) {
        $result | Add-Member -MemberType NoteProperty -Name Statistics -Value $data.statistics
    }

    if ($IncludeRender) {
        $result | Add-Member -MemberType NoteProperty -Name Render -Value $data.render
    }

    $result
}

function GetAccessToken {
    $azureCmdlet = get-command -Name Get-AzContext -ErrorAction SilentlyContinue
    if ($azureCmdlet -eq $null)
    {
        $null = Import-Module Az -ErrorAction Stop;
    }
    $AzureContext = & "Get-AzContext" -ErrorAction Stop;
    $authenticationFactory = New-Object -TypeName Microsoft.Azure.Commands.Common.Authentication.Factories.AuthenticationFactory
    if ((Get-Variable -Name PSEdition -ErrorAction Ignore) -and ('Core' -eq $PSEdition)) {
        [Action[string]]$stringAction = {param($s)}
        $serviceCredentials = $authenticationFactory.GetServiceClientCredentials($AzureContext, $stringAction)
    } else {
        $serviceCredentials = $authenticationFactory.GetServiceClientCredentials($AzureContext)
    }

    # We can't get a token directly from the service credentials. Instead, we need to make a dummy message which we will ask
    # the serviceCredentials to add an auth token to, then we can take the token from this message.
    $message = New-Object System.Net.Http.HttpRequestMessage -ArgumentList @([System.Net.Http.HttpMethod]::Get, "http://foobar/")
    $cancellationToken = New-Object System.Threading.CancellationToken
    $null = $serviceCredentials.ProcessHttpRequestAsync($message, $cancellationToken).GetAwaiter().GetResult()
    $accessToken = $message.Headers.GetValues("Authorization").Split(" ")[1] # This comes out in the form "Bearer <token>"

    $accessToken
}

function GetArmHost {
param(
    [string]
    $environment
    )

    switch ($environment) {
        "" {
            $armHost = "management.azure.com"
        }
        "aimon" {
            $armHost = "management.azure.com"
        }
        "int" {
            $armHost = "api-dogfood.resources.windows-int.net"
        }
    }

    $armHost
}

function BuildUri {
param(
    [string]
    $armHost,
    
    [string]
    $subscriptionId,

    [string]
    $resourceGroup,

    [string]
    $workspaceName,

    [string]
    $queryParams
    )

    "https://$armHost/subscriptions/$subscriptionId/resourceGroups/$resourceGroup/providers/" + `
        "microsoft.operationalinsights/workspaces/$workspaceName/api/query?$queryParamString"
}

function GetHeaders {
param(
    [string]
    $AccessToken,

    [switch]
    $IncludeStatistics,

    [switch]
    $IncludeRender,

    [int]
    $ServerTimeout
    )

    $preferString = "response-v1=true"

    if ($IncludeStatistics) {
        $preferString += ",include-statistics=true"
    }

    if ($IncludeRender) {
        $preferString += ",include-render=true"
    }

    if ($ServerTimeout -ne $null) {
        $preferString += ",wait=$ServerTimeout"
    }

    $headers = @{
        "Authorization" = "Bearer $accessToken";
        "prefer" = $preferString;
        "x-ms-app" = "LogAnalyticsQuery.psm1";
        "x-ms-client-request-id" = [Guid]::NewGuid().ToString();
    }

    $headers
}

function CreateObjectView {
param(
    $data
    )

    # Find the number of entries we'll need in this array
    $count = 0
    foreach ($table in $data.Tables) {
        $count += $table.Rows.Count
    }

    $objectView = New-Object object[] $count
    $i = 0;
    foreach ($table in $data.Tables) {
        foreach ($row in $table.Rows) {
            # Create a dictionary of properties
            $properties = @{}
            for ($columnNum=0; $columnNum -lt $table.Columns.Count; $columnNum++) {
                $properties[$table.Columns[$columnNum].name] = $row[$columnNum]
            }
            # Then create a PSObject from it. This seems to be *much* faster than using Add-Member
            $objectView[$i] = (New-Object PSObject -Property $properties)
            $null = $i++
        }
    }

    $objectView
}


